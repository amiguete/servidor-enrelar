BROKER_URL = "127.0.0.1"
BROKER_PORT = 8886
CA_CERT_PATH = "/root/certificates/ca.cert"
CERT_FILE_PATH = "/root/certificates/client.cert"
KEY_FILE_PATH = "/root/certificates/client.key"
MQTT_USER = ""
MQTT_PASSWORD = ""
DB_PATH = "/root/coordinador/enrelar.db"
