#!/usr/bin/python3
#recibe como argumento o nome dun programa da base de datos
#e lanza tódalas accions previstas para ese programa
import paho.mqtt.client as mqtt #librería de conexión mqtt
import config #ficheiro cas constantes de conexión
import sqlite3 #para traballar ca base de datos
import time #para usar a función sleep()
import sys #para o envío de erros á saída estandar de erros
import signal #para parar adecuadamente o programa

# ------------------------
# Variables globais
# ------------------------
client = mqtt.Client() #cliente usado para conexións MQTT
run = True #para controlar o bucle "infinito"

# ------------------------
# Funcións
# ------------------------
def topic_subscription():
    '''
    - subscripción a topics necesarios
    - definición das funcións que se chamarán en caso de recibir
    mensaxes con ese topics
    '''
    client.subscribe([("deviceOut/+", 0),("program/#", 1)])
    client.message_callback_add("deviceOut/connection", on_message_connection)
    client.message_callback_add("deviceOut/value", on_message_value)
    client.message_callback_add("program/add", on_message_program_add)
    client.message_callback_add("program/del", on_message_program_del)
    client.message_callback_add("program/edit/+", on_message_program_edit)
    client.message_callback_add("program/getList", on_message_program_get)

def on_message_connection(client, userdata, message):
    '''
    - función lanzada cada vez que se recibe unha mensaxe
    co topic deviceOut/connection
    - comproba se existe o módulo e se non insertao. Se existe actualiza
    o alias se é preciso
    '''
    #extraer do topic unha lista con id_modulo, IP, e alias(por ese orden)
    parameter_list = message.payload.decode("utf-8").split("|/|")
    device_id = parameter_list[0]
    #a IP non se ten en conta(futuros usos?)
    #ip = parameter_list[1]
    alias = parameter_list[2]
    try:
        #xerar unha nova conexión(cada thread debe ter a súa propia)
        conn = sqlite3.connect(config.DB_PATH)
        #obter (se existe) a fila correspondente a ese módulo
        sql_sentence = "select id_modulo,alias from modulos where id_modulo=?"
        device_cursor = conn.execute(sql_sentence,(device_id,))
        result = device_cursor.fetchone()
        if result is None: #se non existe engádese á base de datos
            sql_sentence = "insert into modulos(id_modulo, alias, ultimo_valor)"
            sql_sentence += " values (?,?, Null)"
            conn.execute(sql_sentence, (device_id,alias))
        else: #se está na base de datos compróbase e actualízase o alias
            if alias != result[1]:
                sql_sentence = "update modulos set alias=? where id_modulo=?"
                conn.execute(sql_sentence, (alias, device_id))
        device_cursor.close()
        conn.commit()
    except sqlite3.Error as sql_error: #se houbo erro
        conn.rollback() #desfai os cambios
        error_string = "SQLite3 error adding module:\n"
        error_string +="\terror: " + repr(sql_error) + "\n"
        sys.stderr.write(error_string)
        sys.stderr.flush()
    finally: #haxa ou non erro
        conn.close() #pecha a conexión

def on_message_value(client, userdata, message):
    '''
    - función lanzada cada vez que se recibe unha mensaxe
    co topic deviceOut/value
    - modifica o valor de "ultimo_valor" do modulo que enviou a mensaxe
    '''
    #extraer do topic unha lista con id_modulo, e valor (por ese orden)
    parameter_list = message.payload.decode("utf-8").split("|/|")
    device_id = parameter_list[0]
    value = parameter_list[1]
    try:
        #xerar unha nova conexión(cada thread debe ter a súa propia)
        conn = sqlite3.connect(config.DB_PATH)
        #actualizar o contido da columna "ultimo_valor"
        sql_sentence = "update modulos set ultimo_valor=? where id_modulo=?"
        conn.execute(sql_sentence, (value, device_id))
        #engadir o valor ó historial
        sql_sentence = "replace into historial (id_modulo, data_hora,value ) "
        sql_sentence += "values (?, datetime('now', 'localtime'), ?)"
        conn.execute(sql_sentence, (device_id,value))
        conn.commit()
        #cada vez que cambia un valor comprobanse os programas de novo
        #pásase por parametro o tipo de dispositivo
        check_programs(device_id.split("-")[0])
    except sqlite3.Error as sql_error: #se houbo erro
        conn.rollback() #desfai os cambios
        error_string = "SQLite3 error adding value:\n"
        error_string +="\terror: " + repr(sql_error) + "\n"
        sys.stderr.write(error_string)
        sys.stderr.flush()
    finally: #haxa ou non erro
        conn.close() #pecha a conexión

def on_message_program_add(client, userdata, message):
    '''
    - función lanzada cada vez que se recibe unha mensaxe
    co topic program/add
    - lanza a función auxiliar create_program(payload)
    '''
    create_program(message.payload.decode("utf-8"))

def on_message_program_del(client, userdata, message):
    '''
    - función lanzada cada vez que se recibe unha mensaxe
    co topic program/del
    - lanza a función auxiliar delete_program(payload)
    '''
    delete_program(message.payload.decode("utf-8"))

def on_message_program_edit(client, userdata, message):
    '''
    - función lanzada cada vez que se recibe unha mensaxe
    co topic program/edit/N sendo N un numero de programa
    - lanza a función auxiliar delete_program(N)
    - lanza a función auxiliar create_program(payload)
    '''
    id_program = message.topic.split("/")[2]
    delete_program(id_program)
    create_program(message.payload.decode("utf-8"))

def create_program(instructions):
    '''
    - función lanzada para crear un programa novo cas condicións e accións
    codificadas no parámetro de entrada "instructions"
    - exemplo de instrucción con 2 accións:
        "nome|/|horaini|/|horafin|/|dow|/|dataini|/|datafin|@|
        idmodulo|/|value|&|idmodulo2|/|value2|@|
        idmodulo3|/|comparador|/|value”"
    '''
    #separar instruccións
    instructions_list = instructions.split("|@|")
    #separar atributos de programa
    program_atributes = instructions_list[0].split("|/|")
    try:
        #xerar unha nova conexión(cada thread debe ter a súa propia)
        conn = sqlite3.connect(config.DB_PATH)
        #crear programa
        sql_sentence = "insert into programas "
        sql_sentence += "(nome,hora_ini,hora_fin,dia_semana,data_ini,data_fin) "
        sql_sentence += "values (?, ?, ?, ?, ?, ?)"
        cur = conn.execute(sql_sentence,
            (program_atributes[0],program_atributes[1],program_atributes[2],
            program_atributes[3],program_atributes[4],program_atributes[5]))
        id_program = cur.lastrowid
        #se hai accións
        if len(instructions_list[1]) > 0:
            #sepáranse tódalas accións
            actions = instructions_list[1].split("|&|")
            #insertar cada acción na táboa
            for action in actions:
                #separar cada un dos atributos da acción
                atributes = action.split("|/|")
                sql_sentence = "insert into accions "
                sql_sentence += "(id_programa, id_modulo,value) "
                sql_sentence += "values (?, ?, ?)"
                conn.execute(sql_sentence,(id_program,atributes[0],atributes[1]))
        #se hai condicions
        if len(instructions_list[2])>0:
            #sepáranse tódalas condicións
            conditions = instructions_list[2].split("|&|")
            #insertar cada condición na táboa
            for condition in conditions:
                #separar cada un dos atributos da condicion
                atributes = condition.split("|/|")
                sql_sentence = "insert into condicions "
                sql_sentence += "(id_programa, id_modulo, comparador,value ) "
                sql_sentence += "values (?, ?, ?,?)"
                conn.execute(sql_sentence,
                    (id_program,atributes[0],atributes[1],atributes[2]))
        conn.commit()
    except sqlite3.Error as sql_error: #se houbo erro
        conn.rollback() #desfai os cambios
        error_string = "SQLite3 error creating program:\n"
        error_string +="\terror: " + repr(sql_error) + "\n"
        sys.stderr.write(error_string)
        sys.stderr.flush()
    finally: #haxa ou non erro
        conn.close() #pecha a conexión

def delete_program(id_program):
    '''
    - función lanzada para eliminar da BBDD o programa co identificador
    recibido como parámetro de entrada
    '''
    try:
        #xerar unha nova conexión(cada thread debe ter a súa propia)
        conn = sqlite3.connect(config.DB_PATH)
        #borrar o programa indicado
        #facer que o borrado sexa en cascada e borre accions e condicions
        conn.execute("PRAGMA foreign_keys = ON")
        sql_sentence = "delete from programas "
        sql_sentence += "where id_programa = ?"
        conn.execute(sql_sentence,(id_program,))
        conn.commit()
    except sqlite3.Error as sql_error: #se houbo erro
        conn.rollback() #desfai os cambios
        error_string = "SQLite3 error deleting program:\n"
        error_string +="\terror: " + repr(sql_error) + "\n"
        sys.stderr.write(error_string)
        sys.stderr.flush()
    finally: #haxa ou non erro
        conn.close() #pecha a conexión

def on_message_program_get(client, userdata, message):
    '''
    - función lanzada cada vez que se recibe unha mensaxe
    co topic program/getList
    - obten a lista de programas e accións e condicións asociadas
    construe un string e publicao co topic getList/program
    '''
    return_list = "" #lista a devolver
    try:
        #xerar unha nova conexión(cada thread debe ter a súa propia)
        conn = sqlite3.connect(config.DB_PATH)
        #obter lista de programas
        sql_sentence = "select id_programa, nome, hora_ini, hora_fin, "
        sql_sentence += "dia_semana, data_ini,data_fin "
        sql_sentence += "from programas"
        programs_cursor = conn.execute(sql_sentence)
        #de cada programa obtido...
        program_row = programs_cursor.fetchone()
        while program_row is not None:
            #xunta tódolos atributos
            return_list += str(program_row[0]) + "|/|" #id_program
            return_list += program_row[1] + "|/|" #nome
            return_list += program_row[2] + "|/|" #hora_ini
            return_list += program_row[3] + "|/|" #hora_fin
            return_list += program_row[4] + "|/|" #dia_semana
            return_list += program_row[5] + "|/|" #data_ini
            return_list += program_row[6]         #data_fin
            #obtén as accións
            return_list += "|@|" #separador programa/acción
            sql_sentence = "select id_modulo, value "
            sql_sentence += "from accions "
            sql_sentence += "where id_programa = ?"
            actions_cursor = conn.execute(sql_sentence, (program_row[0],))
            #para cada accion
            action_row = actions_cursor.fetchone()
            while action_row is not None:
                #xunta os atributos
                return_list += action_row[0] + "|/|" #id_modulo
                return_list += str(action_row[1])         #value
                #se hai máis accións engade separador &
                action_row = actions_cursor.fetchone()
                if action_row is not None:
                    return_list += "|&|" #nova acción
            #obten as condicións
            return_list += "|@|" #separador acción condición
            sql_sentence = "select id_modulo, comparador, value "
            sql_sentence += "from condicions "
            sql_sentence += "where id_programa = ?"
            conditions_cursor = conn.execute(sql_sentence, (program_row[0],))
            #para cada condición
            condition_row = conditions_cursor.fetchone()
            while condition_row is not None:
                #xunta os atributos
                return_list += condition_row[0] + "|/|" #id_modulo
                return_list += condition_row[1] + "|/|" #comparador
                return_list += str(condition_row[2])          #value
                #se hai máis accións engade separador &
                condition_row = conditions_cursor.fetchone()
                if condition_row is not None:
                    return_list += "|&|" #nova condición
            #se hai máis programas engade o separador ¬
            program_row = programs_cursor.fetchone()
            if program_row is not None:
                return_list += "|¬|"
        client.publish(topic="getList/program", payload=return_list)
        programs_cursor.close()
        actions_cursor.close()
        conditions_cursor.close()
    except sqlite3.Error as sql_error: #se houbo erro SQL
        error_string = "SQLite3 error getting list program:\n"
        error_string +="\terror: " + repr(sql_error) + "\n"
        sys.stderr.write(error_string)
        sys.stderr.flush()
    except Exception as e: #calquera outro erro
        sys.stderr.write(repr(e) + "\n")
        sys.stderr.flush()
    finally: #haxa ou non erro
        conn.close() #pecha a conexión

def check_programs(device_type=None):
    '''
    - obtén unha lista de programas que estén vixentes neste momento
    en función dos parametros de data hora e día da semana
    - para cada programa comproba se ten condicións e se as cumple
    - se non ten condicións ou se cumple as que ten chama a función
    de envío de accións send_actions()
    - pode recibir como parametro o tipo de dispositivo que enviou
    un valor provocando a execución desta comprobación
    - no caso de que o tipo de dispositivo sexa unha botonera
    hai que mudar o seu último valor despóis da comprobación
    para simular un depulsado
    '''
    try:
        #xerar unha nova conexión(cada thread debe ter a súa propia)
        conn = sqlite3.connect(config.DB_PATH)
        #obter lista de programas
        sql_sentence = "select id_programa from programas "
        #que teñan o día da semana de hoxe na súa lista
        sql_sentence += "where dia_semana like strftime('%%%w%%','now','localtime') "
        #que o día de hoxe esté entre os días do ano válidos
        sql_sentence += "and strftime('%m-%d','now','localtime') between "
        sql_sentence += "data_ini and data_fin "
        #e que a hora de agora esté entre as horas do día validas
        sql_sentence += "and strftime('%H:%M','now','localtime') between "
        sql_sentence += "hora_ini and hora_fin"
        programs_cursor = conn.execute(sql_sentence)
        #de cada programa obtido...
        for program_row in programs_cursor:
            is_valid = True
            id_program = str(program_row[0])
            #...obten as codicións asociadas...
            sql_sentence = "select modulos.ultimo_valor, condicions.comparador, "
            sql_sentence += "condicions.value "
            sql_sentence += "from condicions left join modulos "
            sql_sentence += "on condicions.id_modulo = modulos.id_modulo "
            sql_sentence += "where id_programa=?"
            conditions_cursor = conn.execute(sql_sentence,(id_program,))
            #....se algunha non cumple invalida o programa
            for condition_row in conditions_cursor:
                #para os modulos botonera e para os que aínda non teñen o primeiro valor
                if condition_row[0] is None:
                    is_valid = False
                    break
                elif condition_row[1] == "maior":
                    if not condition_row[0] > condition_row[2]:
                        is_valid = False
                        break
                elif condition_row[1] == "igual":
                    if condition_row[0] != condition_row[2]:
                        is_valid = False
                        break
                elif condition_row[1] == "menor":
                    if not condition_row[0] < condition_row[2]:
                        is_valid = False
                        break
            #pechar o cursor
            conditions_cursor.close()
            #se o programa non foi invalidado envíanse as accións
            if is_valid:
                send_actions(id_program)
        programs_cursor.close()
    except sqlite3.Error as sql_error: #se houbo erro
        error_string = "SQLite3 error checking programs:\n"
        error_string +="\terror: " + repr(sql_error) + "\n"
        sys.stderr.write(error_string)
        sys.stderr.flush()
    finally: #haxa ou non erro
        conn.close() #pecha a conexión de check
    #limpar ultimo_valor de botóns para que non figuren como pulsados
    if device_type == "botonera":
        clear_buttons()

def send_actions(id_program):
    '''
    - recibe un identificador do programa que se vai executar
    - obten da base de datos todalas accións a executar
    - envía mensaxes MQTT a todolos modulos cas accións a executar
    '''
    #obter tódalas accións para o programa definido como parametro de entrada
    try:
        #xerar unha nova conexión(cada thread debe ter a súa propia)
        conn = sqlite3.connect(config.DB_PATH)
        sql_sentence = "select accions.id_modulo, accions.value, modulos.ultimo_valor "
        sql_sentence += "from accions left join modulos "
        sql_sentence += "on accions.id_modulo = modulos.id_modulo "
        sql_sentence += "where id_programa=?"
        actions_cursor = conn.execute(sql_sentence, (id_program,))
        #envía unha mensaxe por cada acción obtida
        for action_row in actions_cursor:
            #só no caso de que xa non esté nese estado
            if action_row[1] != action_row[2]:
                #construir o topic co id_modulo/action
                topic_action = action_row[0] + "/action"
                #tomar o valor a enviar
                payload_action = action_row[1]
                client.publish(topic=topic_action, payload=payload_action, qos=1)
        actions_cursor.close()
    except sqlite3.Error as sql_error: #se houbo erro SQL
        error_string = "SQLite3 error sending actions:\n"
        error_string +="\terror: " + repr(sql_error) + "\n"
        sys.stderr.write(error_string)
        sys.stderr.flush()
    except Exception as e: #calquera outro erro
        sys.stderr.write(repr(e) + "\n")
        sys.stderr.flush()
    finally: #haxa ou non erro
        conn.close() #pecha a conexión de send

def clear_buttons():
    '''
    - simula a depulsación dos botóns
    '''
    try:
        conn = sqlite3.connect(config.DB_PATH)
        sql_sentence = "update modulos set ultimo_valor=NULL "
        sql_sentence += "where id_modulo like 'botonera-%'"
        conn.execute(sql_sentence)
        conn.commit()
    except sqlite3.Error as sql_error: #se houbo erro
        conn.rollback() #desfai os cambios
        error_string = "SQLite3 error clearing buttons:\n"
        error_string +="\terror: " + repr(sql_error) + "\n"
        sys.stderr.write(error_string)
        sys.stderr.flush()
    finally: #haxa ou non erro
        conn.close() #pecha a conexión de clear

def handle_stop(signum, frame):
    '''
    cando se recibe un sinal de parar o proceso
    no que corre este programa, cambia o valor
    de run para que se deteña o bucle "infinito"
    '''
    global run
    run = False

# ------------------------
# Execución
# ------------------------
def main():
    '''
    - recepción de mensaxes tanto dos módulos coma dos dispositos móbiles
    - comprobar cada vez que chega un dato dun módulo e cada 30 segundos
    se hai un programa que cumple tódalas condicións e lanza as súas accións
    - xestionar os programas
    '''
    try:
        #xestión de sinais  interprocesos
        #signal.signal(signal.SIGINT, handle_stop)
        signal.signal(signal.SIGTERM, handle_stop)
        #configuración do cliente MQTT
        client.username_pw_set(config.MQTT_USER, config.MQTT_PASSWORD)
        client.tls_set(ca_certs=config.CA_CERT_PATH,
            certfile=config.CERT_FILE_PATH, keyfile=config.KEY_FILE_PATH)
        #conexión do cliente MQTT ó broker
        client.connect(config.BROKER_URL, port=config.BROKER_PORT,
            bind_address=config.BROKER_URL)
        #subscripción a topics
        topic_subscription()
        sys.stdout.write("MQTT client connected and subscription done\n")
        sys.stdout.flush()
        #lanzar un novo fío que atenda a entrada e saída de mensaxes MQTT
        client.loop_start()
        sys.stdout.write("Started thread handling MQTT messages\n")
        sys.stdout.flush()
        #limpar os valores de pulsación de botón por se quedou algún despois dun peche imprevisto
        clear_buttons()
        while run:#comprobar cada 30 segundos por se hai cambios
            #comprobar se hai programas que se deban executar
            check_programs()
            #agardar 30 segundos
            time.sleep(30)
    except Exception as e:
        sys.stderr.write(repr(e) + "\n")
        sys.stderr.flush()
    finally:
        #finalizar o fío de xestión MQTT
        client.loop_stop()
        sys.stdout.write("MQTT thread end\n")
        #desconexión do cliente MQTT
        client.disconnect()
        sys.stdout.write("MQTT client disconnected\n")
        sys.stdout.flush()

# -----------------------------------------------
if __name__ == '__main__':
    main()

