# Servidor Enrelar
Programas e configuracións precisas no servidor do sistema Enrelar. Contén os esquemas relacional e entidade-relación da base de datos; o manual de uso; o esquema da conexión do botón de reinicio e o do reloxo; e un cartafol con tódolos ficheiros de configuración e os programas respetando a árbore de ficheiros linux do sistema raspbian


Aviso!!! non inclúe certificados, é preciso xerar un sistema propio de certificados:
Para crear a CA
    cd /usr/lib/ssl/misc/
    sudo openssl genrsa -out ca_key.pem 1024
    sudo openssl req -x509 -new -nodes -key ca_key.pem -days 4096 -config ca.conf -out ca_cer.pem
    sudo cp ca_cer.pem /root/ca.cert
Para xerar os certificados precisos de servidor e de cliente:
    cd /usr/lib/ssl/misc/
    sudo openssl genrsa -out server_key.pem 1024
    sudo openssl req -out server_req.csr -key server_key.pem -new -config server_8883.conf
    sudo openssl x509 -req -in server_req.csr -out server_cer.pem -sha256 -CAcreateserial -days 4000 -CA ca_cer.pem -CAkey ca_key.pem
    sudo mv server_cer.pem /etc/ssl/private/server_8883.cert
    sudo mv server_key.pem /etc/ssl/private/server_8883.key
    sudo openssl genrsa -out server_key.pem 1024
    sudo openssl req -out server_req.csr -key server_key.pem -new -config server_8884.conf
    sudo openssl x509 -req -in server_req.csr -out server_cer.pem -sha256 -CAcreateserial -days 4000 -CA ca_cer.pem -CAkey ca_key.pem
    sudo mv server_cer.pem /etc/ssl/private/server_8884.cert
    sudo mv server_key.pem /etc/ssl/private/server_8884.key
    sudo openssl genrsa -out server_key.pem 1024
    sudo openssl req -out server_req.csr -key server_key.pem -new -config server_8885.conf
    sudo openssl x509 -req -in server_req.csr -out server_cer.pem -sha256 -CAcreateserial -days 4000 -CA ca_cer.pem -CAkey ca_key.pem
    sudo mv server_cer.pem /etc/ssl/private/server_8885.cert
    sudo mv server_key.pem /etc/ssl/private/server_8885.key
    sudo openssl genrsa -out server_key.pem 1024
    sudo openssl req -out server_req.csr -key server_key.pem -new -config server_8886.conf
    sudo openssl x509 -req -in server_req.csr -out server_cer.pem -sha256 -CAcreateserial -days 4000 -CA ca_cer.pem -CAkey ca_key.pem
    sudo mv server_cer.pem /etc/ssl/private/server_8886.cert
    sudo mv server_key.pem /etc/ssl/private/server_8886.key
    sudo openssl genrsa -out client_key.pem 1024
    sudo openssl req -out client_req.csr -key client_key.pem -new -config client.conf
    sudo openssl x509 -req -in client_req.csr -out client_cer.pem -sha256 -CAcreateserial -days 4000 -CA ca_cer.pem -CAkey ca_key.pem
    sudo openssl pkcs12 -inkey client_key.pem -in client_cer.pem -export -out client.pfx
    sudo mv client_cer.pem /root/client.cert
    sudo mv client_key.pem /root/client.key
    sudo mv client.pfx /root/client.pfx
